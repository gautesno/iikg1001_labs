/** @type {import("express").RequestHandler} */
/** @type {import("raw").RequestHandler} */

const express = require("express");
const app = express();
const port = 8000;
let raw = require("raw-socket");

const ICMP_PROTO = 0x01;
const TCP_PROTO = 0x06;
const UDP_PROTO = 0x11;
const LOCALHOST = [0x7f, 0x00, 0x00, 0x01];
const SOURCE = '10.212.140.72';
const TEST_IP = "10.11.12.127";
const ORDER = [
    0, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19,
    1, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29,
    2, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39,
    3, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49,
    4, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59,
    5, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69,
    6, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79,
    7, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89,
    8, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99,
    9
];
// Nr of packets that should be sent for the firewall to block
const FIREWALL_COUNT = 50;
// Nr of packets to be stopped by ids
const SNORT_COUNT = 40;

const PORTS = [
    "80",   // HTTP
    "443",  // HTTPS
    "22",   // SSH
    "123",  // NTP
    "21",   // FTP
    "53",   // DNS
    "3389", // RDP
    "143",  // IMAP
    "25",   // SMTP
    "43"    // Whois
];

let sourceNum = ipToNum(SOURCE);
let targetNum = [];

app.get('/', (req,res) => {
    res.set('Content-Type', 'text/plain');
    res.send('You have connected to the correct server, but are missing something\n');
});

app.get('/packets', (req,res) => {
    let sourceIP = req.ip;
    targetNum = ipToNum(sourceIP);
    console.log("Request from " + req.ip + " which is " + stoh(req.ip) + " as hex");
    
    let options = {
        protocol: raw.Protocol.ICMP
    };
    
    let socket = raw.createSocket(options);
    
    socket.setOption(raw.SocketLevel.IPPROTO_IP, raw.SocketOption.IP_HDRINCL,
        Buffer.from([0x00, 0x00, 0x00, 0x01]), 4);
    
    socket.on("close", function() {
        console.log("Socket closed");
        process.exit(-1);
    });
    
    socket.on("error", function (error) {
        console.log("error: " + error.toString());
        process.exit(-1);
    });
    
    socket.on("message", function (buffer, source) {
        console.log("Recieved " + buffer.length + " bytes from " + source);
        console.log("   data: " + buffer.toString("hex"));
    });
    
    let buffer = Buffer.from([
        0x45, // Header length 20 bytes
        0x00, // Differentiated services field (DCSP)
        0x00, 0x3c, // Total length
        0x7c, 0x9b, // Identification
        0x00, 0x00, // Fragment offset
        0x80, // TTL
        ICMP_PROTO,  // Protocol ICMP
        0x39, 0x8e, 
        //0xc0, 0xa8, 0x48, 0x53, // Source address 192.168.72.83
        LOCALHOST[0], LOCALHOST[1], LOCALHOST[2], LOCALHOST[3], // source adr
        //0xc0, 0xa8, 0x41, 0x01, // Destination address 192.168.65.1
        targetNum[0], targetNum[1], targetNum[2], targetNum[3], // Dest addr 
        0x08, // ICMP type 8
        0x00, // Code 0
        0x43, 0x52, // Checksum
        0x00, 0x01, // Identifier (BE): 1
        0x0a, 0x09, // Sequence number
        // Data: 32 bytes: abcdefghijklmnopqrstuvwabcdefghi
        0x61, 0x62, 0x63, 0x64, 0x65, 0x66, 0x67, 0x68, 
        0x69, 0x6a, 0x6b, 0x6c, 0x6d, 0x6e, 0x6f, 0x70, 
        0x71, 0x72, 0x73, 0x74, 0x75, 0x76, 0x77, 0x61,
        0x62, 0x63, 0x64, 0x65, 0x66, 0x67, 0x68, 0x69
    ]);
    
    function ping () {
        target = "127.0.0.1";
        socket.send (buffer, 0, buffer.length, sourceIP, function (error,bytes) {
            if (error) {
                console.log(error.toString());
            } else {
                console.log("sent " + bytes + " bytes to " + sourceIP);
            }
        })
    }
    
    ping();
    
    // End the communucation withot sending any data
    res.status(200).end();
});

app.get('/test', (req,res) => {
    let sourceIP = req.ip;
    targetNum = ipToNum(sourceIP);
    fullTest(sourceIP);
    res.status(200).end();
})


app.listen(port, '0.0.0.0', () => {
    console.log(`index.js is listening on port ${port}`);
    
    if (targetNum.length == 0) {
        targetNum = ipToNum('127.0.0.1');
        fullTest('127.0.0.1');
    }
});

// Convert a string IP to array of hex values with length 2
function stoh (ip) {
    
    let splitIP = ip.split('.');
    for (let i = 0; i < splitIP.length; i++) {
        splitIP[i] = atoh(splitIP[i]);
    }
    
    return splitIP;
}

function ipToNum (ip) {
    let splitIP = ip.split('.');
    for (let i = 0; i < splitIP.length; i++) {
        splitIP[i] = Number(splitIP[i]);
    }
    
    return splitIP;
}

function atoh (val) {
    const PADDING = '00';
    let numVal = Number(val);
    let newVal = '';
    
    if (numVal > 256) {
        console.log('Value passed to "atoh" is too large');
        newVal = false;
    } else {
        newVal = (PADDING + Number(val).toString(16)).slice(-2)
    }
    
    return newVal;
}

function toHexPort (portNr) {
    let hexed = [];
    
    if (portNr < 255) {
        hexed.push(0x00);
        hexed.push(portNr);
    } else {
        // add a 0 as padding
        let stringed = ("0" + portNr.toString(16)).slice(-4);
        hexed.push(stringed.slice(0,2), stringed.slice(-2) );
    }
    
    return hexed;
}

// Send 100 packets to target, 10 good, 40 for snort and 50 for ufw
function fullTest(target) {
    let goodList = goodPackets();
    let firewallList = firewallPackets();
    let snortList = snortPackets();
    let packetList = goodList.concat(firewallList, snortList);
    
    let sharedOptions = {
        protocol: UDP_PROTO
    }
    
    //console.log(packetList.length);
    
    for (let i = 0; i < packetList.length; i++) {
        let socket = raw.createSocket(sharedOptions);
        let pktNr = ORDER[i];
        let pts = packetList[pktNr];
        
        if (pts == undefined) {
            console.log("undefined");
            console.log(pktNr);
            console.log(pktOrder);
            console.log(packetList);
        }
        
        //console.log(pktNr);
        
        socket.setOption(raw.SocketLevel.IPPROTO_IP, raw.SocketOption.IP_HDRINCL,
            Buffer.from([0x00, 0x00, 0x00, 0x01]), 4);
        
        socket.send(pts, 0, pts.length, target, function (error, bytes) {
            if (error) {
                console.log(error.toString());
            } else {
                //console.log("sent " + bytes + " bytes to " + target);
            };
        });
    }
}

// Generate 10 packets which should be let through.
function goodPackets() {
    let okeyMsg =  stringToInt("Good packet");
    
    payload = [
        Buffer.from([
            0x45, // Header length 20 bytes
            0x00, // Differentiated services field (DCSP)
            0x00, 0x3c, // Total length
            0x7c, 0x9b, // Identification
            0x00, 0x00, // Fragment offset
            0x80, // TTL
            UDP_PROTO, // Protocol UDP
            0x00, 0x00, // Header checksum placeholder
            // End header
            sourceNum[0], sourceNum[1], sourceNum[2], sourceNum[3], // source adr
            targetNum[0], targetNum[1], targetNum[2], targetNum[3], // Dest adr
            0x1f, 0x40, // Source port 8000
            0x00, 0x50, // Dest port 80
            0x00, 0x13, // Length
            0xb2, 0x0d // UDP Checksum
            // PAYLOAD concated
        ].concat(okeyMsg)),
        Buffer.from([
            0x45, // Header length 20 bytes
            0x00, // Differentiated services field (DCSP)
            0x00, 0x3c, // Total length
            0x7c, 0x9b, // Identification
            0x00, 0x00, // Fragment offset
            0x80, // TTL
            UDP_PROTO, // Protocol UDP
            0x00, 0x00, // Header checksum placeholder
            // End header
            sourceNum[0], sourceNum[1], sourceNum[2], sourceNum[3], // source adr
            targetNum[0], targetNum[1], targetNum[2], targetNum[3], // Dest addr
            0x1f, 0x40, // Source port 8000
            0x01, 0xbb, // Dest port 443
            0x00, 0x13, // Length
            0xb0, 0xa2 // UDP Checksum
            
        ].concat(okeyMsg)),
        Buffer.from([
            0x45, // Header length 20 bytes
            0x00, // Differentiated services field (DCSP)
            0x00, 0x3c, // Total length
            0x7c, 0x9b, // Identification
            0x00, 0x00, // Fragment offset
            0x80, // TTL
            UDP_PROTO, // Protocol UDP
            0x00, 0x00, // Header checksum placeholder
            // End header
            sourceNum[0], sourceNum[1], sourceNum[2], sourceNum[3], // source adr
            targetNum[0], targetNum[1], targetNum[2], targetNum[3], // Dest addr
            0x1f, 0x40, // Source port 8000
            0x00, 0x16, // Dest port 22
            0x00, 0x13, // Length
            0xb2, 0x47 // UDP Checksum
            
        ].concat(okeyMsg)),
        Buffer.from([
            0x45, // Header length 20 bytes
            0x00, // Differentiated services field (DCSP)
            0x00, 0x3c, // Total length
            0x7c, 0x9b, // Identification
            0x00, 0x00, // Fragment offset
            0x80, // TTL
            UDP_PROTO, // Protocol UDP
            0x00, 0x00, // Header checksum placeholder
            // End header
            sourceNum[0], sourceNum[1], sourceNum[2], sourceNum[3], // source adr
            targetNum[0], targetNum[1], targetNum[2], targetNum[3], // Dest addr
            0x1f, 0x40, // Source port 8000
            0x00, 0x7b, // Dest port 123
            0x00, 0x13, // Length
            0xb1, 0xe2  // UDP Checksum
            
        ].concat(okeyMsg)),
        Buffer.from([
            0x45, // Header length 20 bytes
            0x00, // Differentiated services field (DCSP)
            0x00, 0x3c, // Total length
            0x7c, 0x9b, // Identification
            0x00, 0x00, // Fragment offset
            0x80, // TTL
            UDP_PROTO, // Protocol UDP
            0x00, 0x00, // Header checksum placeholder
            // End header
            sourceNum[0], sourceNum[1], sourceNum[2], sourceNum[3], // source adr
            targetNum[0], targetNum[1], targetNum[2], targetNum[3], // Dest addr
            0x1f, 0x40, // Source port 8000
            0x00, 0x15, // Dest port 21
            0x00, 0x13, // Length
            0xb2, 0x48 // UDP Checksum
            
        ].concat(okeyMsg)),
        Buffer.from([
            0x45, // Header length 20 bytes
            0x00, // Differentiated services field (DCSP)
            0x00, 0x3c, // Total length
            0x7c, 0x9b, // Identification
            0x00, 0x00, // Fragment offset
            0x80, // TTL
            UDP_PROTO, // Protocol UDP
            0x00, 0x00, // Header checksum placeholder
            // End header
            sourceNum[0], sourceNum[1], sourceNum[2], sourceNum[3], // source adr
            targetNum[0], targetNum[1], targetNum[2], targetNum[3], // Dest addr
            0x1f, 0x40, // Source port 8000
            0x00, 0x35, // Dest port 53
            0x00, 0x13, // Length
            0x00, 0x00 // UDP Checksum
            
        ].concat(okeyMsg)),
        Buffer.from([
            0x45, // Header length 20 bytes
            0x00, // Differentiated services field (DCSP)
            0x00, 0x3c, // Total length
            0x7c, 0x9b, // Identification
            0x00, 0x00, // Fragment offset
            0x80, // TTL
            UDP_PROTO, // Protocol UDP
            0x00, 0x00, // Header checksum placeholder
            // End header
            sourceNum[0], sourceNum[1], sourceNum[2], sourceNum[3], // source adr
            targetNum[0], targetNum[1], targetNum[2], targetNum[3], // Dest addr
            0x1f, 0x40, // Source port 8000
            0x0d, 0x3d, // Dest port 3389
            0x00, 0x13, // Length
            0x00, 0x00 // UDP Checksum
            
        ].concat(okeyMsg)),
        Buffer.from([
            0x45, // Header length 20 bytes
            0x00, // Differentiated services field (DCSP)
            0x00, 0x3c, // Total length
            0x7c, 0x9b, // Identification
            0x00, 0x00, // Fragment offset
            0x80, // TTL
            UDP_PROTO, // Protocol UDP
            0x00, 0x00, // Header checksum placeholder
            // End header
            sourceNum[0], sourceNum[1], sourceNum[2], sourceNum[3], // source adr
            targetNum[0], targetNum[1], targetNum[2], targetNum[3], // Dest addr
            0x1f, 0x40, // Source port 8000
            0x00, 0x8f, // Dest port 143
            0x00, 0x13, // Length
            0x00, 0x00 // UDP Checksum
            
        ].concat(okeyMsg)),
        Buffer.from([
            0x45, // Header length 20 bytes
            0x00, // Differentiated services field (DCSP)
            0x00, 0x3c, // Total length
            0x7c, 0x9b, // Identification
            0x00, 0x00, // Fragment offset
            0x80, // TTL
            UDP_PROTO, // Protocol UDP
            0x00, 0x00, // Header checksum placeholder
            // End header
            sourceNum[0], sourceNum[1], sourceNum[2], sourceNum[3], // source adr
            targetNum[0], targetNum[1], targetNum[2], targetNum[3], // Dest addr
            0x1f, 0x40, // Source port 8000
            0x00, 0x19, // Dest port 25
            0x00, 0x13, // Length
            0x00, 0x00 // UDP Checksum
            
        ].concat(okeyMsg)),
        Buffer.from([
            0x45, // Header length 20 bytes
            0x00, // Differentiated services field (DCSP)
            0x00, 0x3c, // Total length
            0x7c, 0x9b, // Identification
            0x00, 0x00, // Fragment offset
            0x80, // TTL
            UDP_PROTO, // Protocol UDP
            0x00, 0x00, // Header checksum placeholder
            // End header
            sourceNum[0], sourceNum[1], sourceNum[2], sourceNum[3], // source adr
            targetNum[0], targetNum[1], targetNum[2], targetNum[3], // Dest addr
            0x1f, 0x40, // Source port 8000
            0x00, 0x2b, // Dest port 43
            0x00, 0x13, // Length
            0x00, 0x00 // UDP Checksum
            
        ].concat(okeyMsg))
    ];
    
    for (let i = 0; i < payload.length; i++) {
        let headerChecksum = raw.createChecksum(payload[i]);
        raw.writeChecksum(payload[i], 2, headerChecksum);
        //raw.writeChecksum(payload[i], 26, (0xa00 + headerChecksum));
        //raw.writeChecksum(payload[i], 26, 0xb20d);
    }
    
    return payload;
}

// Generate 50 packets to be stopped by firewall
function firewallPackets() {
    let fireMsg = stringToInt("This should be blocked by firewall.");
    let payloadPre = [
            0x45, // Header length 20 bytes
            0x00, // Differentiated services field (DCSP)
            0x00, 0x3c, // Total length
            0x7c, 0x9b, // Identification
            0x00, 0x00, // Fragment offset
            0x80, // TTL
            UDP_PROTO, // Protocol UDP
            0x00, 0x00, // Header checksum INCORRECT
            // End header
            sourceNum[0], sourceNum[1], sourceNum[2], sourceNum[3], // source adr
            targetNum[0], targetNum[1], targetNum[2], targetNum[3], // Dest addr
            0x1f, 0x40, // Source port 8000
    ];
    let payload = [];
    
    // Generate FIREWALL_COUNT packages where ports are not in PORTS
    for (let i = 0; i < FIREWALL_COUNT; i++) {
        let targetPort = Math.floor(Math.random() * 65000);
        let portHex = [];
        
        // If the port chosen is meant to be permitted, generate a new one.
        while (PORTS.includes(targetPort)) {
            targetPort = Math.floor(Math.random() * 65000);
        }
        
        // Convert targetPort to hex values in array of length 2
        portHex = toHexPort(targetPort);
        
        // Apped buffer of payload to payload array
        payload.push(Buffer.from(payloadPre.concat(
            portHex, [
                0x00, 0x2b, // length
                0x00, 0x00 // checksum
            ], 
            fireMsg
        )));
    }
    
    // Generate FIREWALL_COUNT/2 packets where the ip is not allowed.
    
    
    //console.log(payload);
    
    // Return the array of Buffer objects
    return payload;
}

// Generate 40 packets to be stopped by snort
function snortPackets() {
    let wrongChecksum = snortChecksum(); // 10 with wrong checksum
    let wrongLength = snortLength(); // 10 with wrong length
    let badHash = snortHash(); // 10 with bad hash
    let badString = snortString(); // 10 with "bad" string
    let allPackets = wrongChecksum.concat(wrongLength, badHash, badString);
    
    return allPackets;
}

// 10 packets with incorrect header checksums
function snortChecksum () {
    let checksumMsg = stringToInt("This packet has a bad checksum");
    let payload = [];
    let payloadPre = [
        0x45, // Header length 20 bytes
        0x00, // Differentiated services field (DCSP)
        0x00, 0x3c, // Total length
        0x7c, 0x9b, // Identification
        0x00, 0x00, // Fragment offset
        0x80, // TTL
        UDP_PROTO, // Protocol UDP
        0x00, 0x00, // Header checksum INCORRECT
        // End header
        sourceNum[0], sourceNum[1], sourceNum[2], sourceNum[3], // source adr
        targetNum[0], targetNum[1], targetNum[2], targetNum[3], // Dest addr
        0x1f, 0x40, // Source port 8000
    ];
    
    for (let i = 0; i < (SNORT_COUNT / 4); i++) {
        payload.push(Buffer.from(payloadPre.concat(
            toHexPort(PORTS[i]), [
                0x00,0x08, // length
                0x00,0x00 // checksum INCORRECT
            ],
            checksumMsg
        )));
    }
    
    return payload;
}

// 10 packets with incorrect lengths
function snortLength () {
    let lengthMsg = stringToInt("This packet has incorrect length");
    let payload = [];
    let payloadPre = [
        0x45, // Header length 20 bytes
        0x00, // Differentiated services field (DCSP)
        0x00, 0x3c, // Total length
        0x7c, 0x9b, // Identification
        0x00, 0x00, // Fragment offset
        0x80, // TTL
        UDP_PROTO, // Protocol UDP
        0x00, 0x00, // Header checksum INCORRECT
        // End header
        sourceNum[0], sourceNum[1], sourceNum[2], sourceNum[3], // source adr
        targetNum[0], targetNum[1], targetNum[2], targetNum[3], // Dest addr
        0x1f, 0x40, // Source port 8000
    ];
    
    for (let i = 0; i < (SNORT_COUNT / 4); i++) {
        payload.push(Buffer.from(payloadPre.concat(
            toHexPort(PORTS[i]), [
                0x00,0x08, // length
                0x00,0x00 // checksum INCORRECT
            ],
            lengthMsg
        )));
    }
    
    return payload;
}

// 10 packets with incorrect packet hashes
function snortHash () {
    let hashMsg = stringToInt("This packet has a bad hash");
    let payload = [];
    let payloadPre = [
        0x45, // Header length 20 bytes
        0x00, // Differentiated services field (DCSP)
        0x00, 0x3c, // Total length
        0x7c, 0x9b, // Identification
        0x00, 0x00, // Fragment offset
        0x80, // TTL
        UDP_PROTO, // Protocol UDP
        0x00, 0x00, // Header checksum INCORRECT
        // End header
        sourceNum[0], sourceNum[1], sourceNum[2], sourceNum[3], // source adr
        targetNum[0], targetNum[1], targetNum[2], targetNum[3], // Dest addr
        0x1f, 0x40, // Source port 8000
    ];
    
    for (let i = 0; i < (SNORT_COUNT / 4); i++) {
        payload.push(Buffer.from(payloadPre.concat(
            toHexPort(PORTS[i]), [
                0x00,0x08, // length
                0x00,0x00 // checksum INCORRECT
            ],
            hashMsg
        )));
    }
    
    return payload;
}

// 10 packets with "bad" strings
function snortString () {
    let stringMsg = stringToInt('This packet contains a "bad" string. webshell.exe. Should have been stopped or alerted by snort');
    let payload = [];
    let payloadPre = [
        0x45, // Header length 20 bytes
        0x00, // Differentiated services field (DCSP)
        0x00, 0x3c, // Total length
        0x7c, 0x9b, // Identification
        0x00, 0x00, // Fragment offset
        0x80, // TTL
        UDP_PROTO, // Protocol UDP
        0x00, 0x00, // Header checksum INCORRECT
        // End header
        sourceNum[0], sourceNum[1], sourceNum[2], sourceNum[3], // source adr
        targetNum[0], targetNum[1], targetNum[2], targetNum[3], // Dest addr
        0x1f, 0x40, // Source port 8000
    ];
    
    for (let i = 0; i < (SNORT_COUNT / 4); i++) {
        payload.push(Buffer.from(payloadPre.concat(
            toHexPort(PORTS[i]), [
                0x00,0x08, // length
                0x00,0x00 // checksum INCORRECT
            ],
            stringMsg
        )));
    }
    
    return payload;
}


function stringToInt(string) {
    let converted = [];
    
    for (let i = 0; i < string.length; i++) {
        converted[i] = string.charCodeAt(i);
    }
    
    return converted;
}


function testUDP (target) {
    let options = {
        //protocol: raw.Protocol[17]
        protocol: UDP_PROTO // UDP
    };
    
    let socket = raw.createSocket(options);
    
    let buffer = Buffer.from([
        0x45, // Header length 20 bytes
        0x00, // Differentiated services field (DCSP)
        0x00, 0x3c, // Total length
        0x7c, 0x9b, // Identification
        0x00, 0x00, // Fragment offset
        0x80, // TTL
        0x11, // Protocol UDP
        0x00, 0x00, // Checksum NOT CORRECT
        LOCALHOST[0], LOCALHOST[1], LOCALHOST[2], LOCALHOST[3], // source adr
        LOCALHOST[0], LOCALHOST[1], LOCALHOST[2], LOCALHOST[3], // Dest addr 
        0x00, 0x00, // Source port
        0x00, 0x00, // Dest port
        0x00, 0x00, // Length
        0x00, 0x00, // Checksum INCORRECT
        // PAYLOAD
    ].concat([0x01,0x02]));
    
    socket.setOption(raw.SocketLevel.IPPROTO_IP, raw.SocketOption.IP_HDRINCL,
        Buffer.from([0x00, 0x00, 0x00, 0x01]), 4);
    
    console.log(socket);
        
    socket.send(buffer, 0, buffer.length, target, function (error, bytes) {
        if (error) {
            console.log(error.toString());
        } else {
            console.log("sent " + bytes + " bytes to " + target);
        };
    });
}
