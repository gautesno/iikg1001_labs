#!/usr/bin/env bash

sudo apt update

sudo apt install -y \
    ca-certificates \
    curl \
    gnupg \
    lsb-release

sudo mkdir -p /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg

echo \
"deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
  
sudo apt update 

sudo apt install -y docker-ce docker-ce-cli containerd.io docker-compose-plugin

# Add our user do the docker group to 
# allow docker commands without super user privileges. 
# This impacts Docker Daemon Attack Surface

# Create docker group
sudo groupadd docker

# Add our user to the docker group
sudo usermod -aG docker $USER

# Activate changes to groups
newgrp docker