#!/usr/bin/env bash

# Snort Install, Run And Enter
# Script based on guide from: https://hub.docker.com/r/ciscotalos/snort3

# Check if docker is installed, exit if not
if apt show docker-ce | grep -q 'Installed: yes'; then
    echo "Docker CE is installed, proceeding"
else 
    echo "Docker CE is not installed, exiting..."
    exit
fi

# Download container if not downloaded
if docker images | grep -q 'snort3'; then
    echo "Snort3 already downloaded"
else 
    docker pull ciscotalos/snort3
fi

# Create container network
if docker inspect sn0 | grep -q 'Name:'; then
    echo "Network already exists"
else 
    docker network create --subnet 127.20.0.0/24 sn0
fi 

# Start container if not running
if docker container ls | grep -q 'snort3'; then
    echo "Snort3 is already running"
else 
    docker run --name snort3 -h snort3 -u snorty -w /home/snorty -d -it ciscotalos/snort3 bash
fi

# Enter container
docker exec -it snort3 bash