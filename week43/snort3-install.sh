#!/usr/bin/env bash

# Commands based on: https://kifarunix.com/install-and-configure-snort-3-on-ubuntu/

## Ensure packets are updated
sudo apt update
sudo apt upgrade -y

## Install required build tools
sudo apt install build-essential libpcap-dev libpcre3-dev \
libnet1-dev zlib1g-dev luajit hwloc libdnet-dev \
libdumbnet-dev bison flex liblzma-dev openssl libssl-dev \
pkg-config libhwloc-dev cmake cpputest libsqlite3-dev uuid-dev \
libcmocka-dev libnetfilter-queue-dev libmnl-dev autotools-dev \
libluajit-5.1-dev libunwind-dev libfl-dev -y

## Make directory for snort daq
mkdir ~/snort3-source && cd ~/snort3-source

git clone https://github.com/snort3/libdaq.git
cd libdaq

sudo ./bootstrap
sudo ./configure
sudo make 
sudo make install

# Install snort 3
cd 
wget https://github.com/snort3/snort3/archive/refs/tags/3.1.28.0.tar.gz
tar xzf 3.1.28.0.tar.gz
cd snort3-3.1.28.0
sudo ./configure_cmake.sh --prefix=/usr/local 
cd build
sudo make
sudo make install

# Update shared libraries
sudo ldconfig

# Turn on promiscuous mode for ethernet interface
sudo ip link set dev ens3 promisc on

# Disable interface offloading
sudo ethtool -K ens3 gro off lro off

# Create a daemon to ensure NIC changes are persistant across system reboot
cd ~
cat > snort3-nic.service << 'EOL'
[Unit]
Description=Set Snort 3 NIC in promiscuous mode and Disable GRO, LRO on boot
After=network.target

[Service]
Type=oneshot
ExecStart=/usr/sbin/ip link set dev ens3 promisc on
ExecStart=/usr/sbin/ethtool -K ens3 gro off lro off
TimeoutStartSec=0
RemainAfterExit=yes

[Install]
WantedBy=default.target
EOL

sudo mv snort3-nic.service /etc/systemd/system/

sudo systemctl daemon-reload

# Enable the NIC service change on boot
sudo systemctl enable --now snort3-nic.service

# Create a folder for snort rules
mkdir ~/rules

# Create symlink for the rules folder
sudo ln -s /home/ubuntu/rules /usr/local/etc/rules